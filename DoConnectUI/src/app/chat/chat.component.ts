import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { ChatService } from './../chat.service';
import { MessageDTO } from './../messageDTO';
import { UserService } from './../user.service';
import { Router } from '@angular/router';
import {MatCardModule} from '@angular/material/card';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit
{
  constructor() { }

  ngOnInit(): void { }
}